//  Copyright (C) 2019 EXTOLL GmbH.
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 3 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, see
//  <http://www.gnu.org/licenses/>.

module tb_top;

reg clk;

initial begin
    forever begin
        clk <= 1'b0;
        #5ns;
        clk <= 1'b1;
        #5ns;
    end
end

endmodule : tb_top
