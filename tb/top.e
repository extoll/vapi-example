Copyright (C) 2019 EXTOLL GmbH.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, see
<http://www.gnu.org/licenses/>.

<'

extend sys {

    clk_p : in event_port is instance;
    keep clk_p.hdl_path() == "tb_top.clk";
    keep clk_p.edge() == rise;

    event test_e;
    example_cov : uint(bits:4);

    error: bool;

    start_test() @sys.any is {
        message(LOW,"Starting test");

        for{var i := 0; i < 100; i +=1} {
            wait @clk_p$;
        };

        emit test_e;

        if(error == TRUE) {
            dut_error("Found an error");
        };

        message(LOW,"Test done");
        stop_run();
    };

    run() is also {
        start start_test();
    };

    cover test_e is {
        item test : uint(bits:4) = example_cov;
    };
};

'>
