#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from vapi import *

from vapi.models.containers import *
from vapi.models.filters import *

from vapi.models.enums import ConnectType

#from jinja2 import Environment, FileSystemLoader, select_autoescape
from pathlib import Path
import os
import sys
import time

server = "vmanager.example.com"
port = 1313

vapi_user = "changeme"
vapi_password = "changeme"
vapi_project = "vmgr"

password_env_variable = "HOST_PASSWORD"
password_file = "password"

unix_username = "changeme"
unix_password = ""

if password_env_variable in os.environ:
    unix_password = os.environ.get(password_env_variable)
    print("Using password from env variable")

if Path(password_file).is_file():
    file = open(password_file).read().splitlines()
    unix_password = file[0]
    print("Using password from file")

workspace = os.path.dirname(os.path.realpath(__file__))
print("Workspace dir: ", workspace)

vsif = workspace + "/vsif/test.vsif"

env_vars = {"WORKSPACE": workspace, "VMGR_EXAMPLE": workspace}

credential = Credentials(username=unix_username, password=unix_password, connectType=ConnectType.PASSWORD)

con = Connection(server, port, vapi_user, vapi_password, vapi_project)

sessions = Sessions(con)
runs = Runs(con)
reports = Reports(con)

# -- create session directory. Must be writable by user running vManager.
regressionDir = os.path.join(workspace, "regression")
if not os.path.exists(regressionDir):
    os.makedirs(regressionDir)
    os.chmod(regressionDir, 0o777)

# ----------------------------------------------------------------------------------------------------------------------
# launch regression ----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

launch_request = LaunchRequest(
    vsif=vsif,
    environment=env_vars,
    credentials=credential,
)

session_id = sessions.launch(launch_request=launch_request)

session = sessions.get(session_id)
print("Started session {0} with ID {1:d}".format(session.name, session_id))
print()

session = sessions.wait(session_id, enable_printout=True)

session_runs = runs.list(
    SPGFRetrievalSpecification(
        filter=InFilter("parent_session_name", InFilterOperand.IN, session.name)
    )
)

print("Run count: "+str(len(session_runs)))

failed_runs = runs.list(
    SPGFRetrievalSpecification(
        filter=And(
            InFilter("parent_session_name", InFilterOperand.IN, session.name),
            InFilter("status",              InFilterOperand.IN, "failed")
        )
    )
)

print("Failed run count: "+str(len(failed_runs)))

# ----------------------------------------------------------------------------------------------------------------------
# Generate reports -----------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------
# WARNING: Make sure, that the report directories are writable by the user running vManager. Here we make them world
#          writable for convenience, which is not the best idea security wise!
# ----------------------------------------------------------------------------------------------------------------------

topReportDir = os.path.join(workspace, "reports")
if not os.path.exists(topReportDir):
    os.makedirs(topReportDir)
    os.chmod(topReportDir, 0o777)

reportList = []

sessionDir = os.path.join(topReportDir, "session")
if not os.path.exists(sessionDir):
    os.makedirs(sessionDir)
    os.chmod(sessionDir, 0o777)

report = reports.generate_sessions_report(HTMLReportRequest(
     rs=GFRetrievalSpecification(filter=InFilter(attName="session_name", operand=InFilterOperand.IN, values=session.name)),
     topDir=topReportDir,
     reportDir="session",
     override=True
 ))

print("Session report written to ", report.path)
reportList.append({"dir": "session/index.html", "name": "Session Report"})


vplan = Vplan(con)

val = str(int(time.mktime((2019, 4, 16, 10, 25, 0, 0, 0, 0))) * 1000)

vplanReport = vplan.generate_html_report(
    HTMLVplanReportRequest(
        ctxData=ReportContextData(
            vplanFile=workspace + "/vsif/example.vplanx",
        ),
        topDir=topReportDir,
        reportDir="vplan",
        override=True,
        detailed=True,
        rs=GFRetrievalSpecification(
            filter=And(
                And(
                    RelationFilter(
                        relationName="session",
                        filter=ExpressionFilter("name", "vmgrtest*")
                    ),
                    RelationFilter(
                        relationName="session",
                        filter=AttValueFilter("owner", AttValueFilterOperand.EQUALS, unix_username),
                    ),
                    RelationFilter(
                        relationName="session",
                        filter=AttValueFilter("start_time", AttValueFilterOperand.GREATER_OR_EQUALS_TO, val),
                    ),
                )
            )
        )
    ),
    routing_retain=False
)

print("VPlan report written to ", vplanReport.path)
reportList.append({"dir": "vplan/index.html", "name": "VPlan Report"})

# ----------------------------------------------------------------------------------------------------------------------
# Generate index.html --------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------

#env = Environment(
#    loader=FileSystemLoader("scripts"),
#    autoescape=select_autoescape(['html'])
#)
#template = env.get_template("index.html")

#template.stream(
#    date=datetime.now().strftime("%c"),
#    reports=reportList
#).dump(os.path.join(topReportDir, "index.html"))

if session.failed():
    print("Run failed")
    sys.exit(1)
