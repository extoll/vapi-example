#!/usr/bin/env bash

#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

echo "Starting compile"

hostname

#-- Workaround
unset TMPDIR
#-- end workaround

xrun -ERROrmax 1 -verbose \
	${BRUN_CODE_COVERAGE} \
	-licqueue \
	-c -xmlibdirname ${BRUN_SESSION_DIR}/INCA_libs \
	-sncompargs -enable_DAC \
	-access r \
	${WORKSPACE}/tb/tb_top.sv \
	${WORKSPACE}/tb/top.e \
	$*

