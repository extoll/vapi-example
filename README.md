# Setup Environment

Prerequisites
* Access to a Cadence® vManager™ instance

## Clone this project

    $ git clone https://gitlab.com/extoll/vapi-example.git
    $ cd vapi-example

## Create a python virtual environment

    $ python3 -m venv venv
    
## Activate virtual environment

    $ source venv/bin/activate
    
## Install vAPI into virtual environment

Either by pip

    $ pip install git+https://gitlab.com/extoll/vapi.git#egg=vapi

or directly from a GIT checkout

    $ git clone git@gitlab.com:extoll/vapi.git
    $ cd vapi
    $ python setup.py install
    
# Run examples

Before running the regression, the environment variable WORKSPACE must be set:

    $ export WORKSPACE=`pwd`

Also have a look at regression.py. Adjust the vmanager server name, the user names
and the passwords to your local setup.

Run the complete regression example:

    $ python regression.py
